#!/usr/bin/env bash

# get utils
. /etc/hondo/util.sh || { echo "ERROR:Couldn't find util script."; exit 1; }

# parse for arguments
get_args="
from argparse import ArgumentParser as AP
from argparse import Namespace as NS
parser=AP(prog='hondo',epilog='Created December 2020',description='Package manager for Queen Anne\'s OS')
sp=parser.add_subparsers(title='commands',dest='command',metavar='command',help='(description)',description='pass the \'-h\' parameter to any command to get additional help')
# the 'list' command
list=sp.add_parser(
    'list',
    epilog='Created December 2020',
    help='List packages in the package database',
    description='List packages in the package database'
)
# the 'sync' command
sync=sp.add_parser(
    'sync',
    epilog='Created December 2020',help='Sync all packages in database to current versions',
    description='Sync all packages in database to current versions'
)

# parser args and update environment
try: args=parser.parse_args()
except: exit(1)
if not args.command: 
    parser.print_usage(); exit(1)
for var,val in args._get_kwargs(): print('%s=%s'%(var,val))
"
ENV=$(python3 -c "$get_args" $@) || exit $?
eval "$ENV"

# run the specified command (fake)
# echo "Running command: $command"
lsh . /etc/hondo/$command.sh || error "Failed to load command config."
$command
# args="$@"
# sudo -u hondo -g media /bin/bash -l -c "$args"