## utils for the 'list' command

# get global utils
. /etc/hondo/util.sh

# list packages with versions
names="
from sys import stdin
for line in stdin: print(line.strip().split()[1])"
function list() {
    # check for database validity
    check_db_exists || exit 1
    # get package list
    packages=$(list_database) || error "Failed to fetch package list"
    plist=$(echo "$packages" | python -c "$names")
    # get versions
    estart "Looking for package versions"
    pvers=;
    for pkg in $plist; do 
        { version=$(lesh hmake -C $QAOSDIR/$pkg version); } || version="(UNKNOWN)"
        pvers="$pkg:$version $pvers"; 
    done
    eok
    echo
    WIDTH=18
    python -c "print('Package Name'.ljust($WIDTH)+'Package Version'.rjust($WIDTH))"
    python -c "print(('-'*($WIDTH-1)).ljust($WIDTH)+('-'*($WIDTH-1)).rjust($WIDTH))"
    for pkg_ver in $pvers; do 
        python -c "pkg,ver='$pkg_ver'.split(':'); print('%s%s'%(pkg.ljust($WIDTH),ver.rjust($WIDTH)))"
    done
    

}