# README #

The `hondo` utility is meant to manage the `qaos` tree, a la `emerge` for gentoo's `portage` tree. Simply run `hondo` from the command line.

> **Note:** Current version is just a suid wrapper to the *hondo* user

### Overview ###

* Util for managing the qaos tree (/usr/qaos). Inspired by gentoo's 'emerge' for managing the portage tree (/usr/portage).
* v0.1.0

### Installation ###

* clone repository url
* run the `autogen.sh` script
* `make`, `sudo make install`
