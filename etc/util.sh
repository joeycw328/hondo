# global variables
QAOSDIR=/usr/qaos
LOG=/var/log/qaos/hondo.log
ELOG=/var/log/qaos/hondo.error.log
CFG=/etc/hondo

# create the log dir if it doesn't exist
{ test -d $(dirname $LOG) || mkdir -p $(dirname $LOG); } || { echo "ERROR: $(dirname $(dirname $LOG)) inaccessible"; exit 1; }
# empty logfiles
echo -n "" > $ELOG || { echo "Failed to setup logfile.";  exit 1; }
echo -n "" > $LOG || { echo "Failed to setup logfile.";  exit 1; }

# run command with output logged
function lesh() {
    2>>$ELOG "$@"; return $?;
}
function lsh() {
    >>$LOG lesh "$@"; return $?;
}

# write error msg and exit
function error(){
    echo "ERROR: $1"
    echo "See $ELOG for details."
    exit 1
}

function estart() { echo -n "$1..."; }
function eok(){ echo "ok."; }
function efail() { echo "failed."; test -n "$1" && echo "$1"; }

# make sure package database exists
noexist="QAOS tree not found or inaccessible. Run:
    hondo sync
To populate the QAOS tree"
corrupt="QAOS tree corrupt or uninitialized. Run:
    hondo sync
To re-populate the QAOS tree"
function check_db_exists() {
    estart "Validating database integrity"
    # check for the chaos directory
    lsh test -d $QAOSDIR || { efail "$noexist"; exit 1; }
    # check for a list of submodules
    lsh pushd $QAOSDIR || { efail; error "Database access denied."; }
    2>>$ELOG output=$(git submodule) || { efail "$corrupt"; exit 1; }
    &>>$LOG echo "found packages: $output"
    lsh test -n "$output" || { efail "$corrupt"; exit 1; }
    lsh popd
    eok
}

# get raw list of all packages with metadata
# note: output is one line per package, where line is
#   <+/-> <commit_hash> <pkg> <branch>
function list_database() {
    # cd to package dir and get package list
    lsh pushd $QAOSDIR || error "Failed to access database."
    git submodule
    lsh popd
}

# run make with hondo utilities included
function hmake() {
    make -f $CFG/hondo.mk --no-print-directory $@
}