# include specific, and shared makefiles
include Makefile
include /opt/qaos-config/common.mk

.ONESHELL:

## define workhorse targets

## define aux utilities

# simply print the package version
version:
	@echo "$(PACKAGE_VERSION)"