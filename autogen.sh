#!/usr/bin/env bash

autoreconf --force --verbose --install && ./configure --prefix=/ --sysconfdir=/etc --exec-prefix=/usr